﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inbox.Application.Dtos;
using Inbox.Application.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Inbox.Controllers
{
    [ApiController]
    [Route("inbox")]
    public class InboxController : ControllerBase
    {
        private readonly IInboxService _service;

        public InboxController(IInboxService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<InboxMessageListDto>> Inbox()
        {
            return Ok(await _service.GetAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<InboxMessageDto>> Inbox(Guid id)
        {
            return Ok(await _service.Get(id));
        }

        [HttpPatch("{id}/read")]
        public async Task<ActionResult> Read(Guid id)
        {
            await _service.MakeRead(id);
            return NoContent();
        }

        [HttpPatch("{id}/archive")]
        public async Task<ActionResult> Archive(Guid id)
        {
            await _service.MakeArchived(id);
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult> AddNew(CreateInboxMessageDto dto)
        {
            await _service.AddNew(dto);
            return NoContent();
        }
    }
}