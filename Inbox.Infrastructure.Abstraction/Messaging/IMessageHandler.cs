using System.Threading.Tasks;

namespace Inbox.Infrastructure.Abstraction.Messaging
{
    public interface IMessageHandler<T> where T : class, new()
    {
        Task Handle(T message);
    }
}