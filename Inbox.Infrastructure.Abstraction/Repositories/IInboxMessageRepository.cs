using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inbox.Domain.Models;

namespace Inbox.Infrastructure.Abstraction.Repositories
{
    public interface IInboxMessageRepository
    {
        Task<IEnumerable<InboxMessageModel>> FindAll();
        Task<InboxMessageModel> Find(Guid id);
        Task Delete(Guid id);
        Task Update(InboxMessageModel model);
        Task Create(InboxMessageModel model);
    }
}