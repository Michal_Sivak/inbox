# Inbox application

## Config

All configuration files can be found in `Inbox.Infrastructure.Configs` directory

You can configure them with User-Secrets for instance but there are numerous other possibilities

## Setup

### Database

I am using Azure SQL. Just pass `ConnectionString` to `DatabaseConfig` 
and start migration

### RabbitMq

Pass `ConnectionString` to `MessagingConfig`

I am running RabbitMq with Docker because of simplicity
and then connecting to that host. It also offers administration tool

Use this command if you want:

```bash
docker run -d --hostname my-rabbit --name some-rabbit -p 8080:15672 -p 5672:5672 rabbitmq:3-management
```

Queue is named `rabbitmq-test` in `appsettings.json`

So you need to either rename it or create a queue with this name