using System;
using Inbox.Infrastructure.Abstraction.Repositories;
using Inbox.Infrastructure.Configs;
using Inbox.Infrastructure.Messaging;
using Inbox.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RabbitMQ.Client;

namespace Inbox.Infrastructure.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration configuration)
        {
            var databaseConfig = configuration.GetSection("DatabaseConfig").Get<DatabaseConfig>();
            services.AddDbContext<InboxContext>(builder =>
            {
                builder.UseSqlServer(databaseConfig.ConnectionString);
            });

            services.AddScoped<IInboxMessageRepository, InboxMessageRepository>();

            var messagingConfig = configuration.GetSection("MessagingConfig").Get<MessagingConfig>();
            services.AddSingleton(_ => new ConnectionFactory()
            {
                Uri = new Uri(messagingConfig.ConnectionString),
                DispatchConsumersAsync = true
            });
            services.AddSingleton(messagingConfig);

            services.AddTransient<IMessageReceiver, MessageReceiver>();

            return services;
        }
    }
}