using Inbox.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Inbox.Infrastructure
{
    public class InboxContext : DbContext
    {
        public InboxContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<InboxMessageModel> Messages { get; set; }
    }
}