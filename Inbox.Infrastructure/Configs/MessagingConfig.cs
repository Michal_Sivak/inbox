namespace Inbox.Infrastructure.Configs
{
    public class MessagingConfig
    {
        public string ConnectionString { get; set; }
        public string QueueName { get; set; }
    }
}