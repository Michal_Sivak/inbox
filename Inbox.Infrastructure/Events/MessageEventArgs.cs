using System;

namespace Inbox.Infrastructure.Events
{
    public class MessageEventArgs : EventArgs
    {
        public string DtoType { get; set; }
        public string Body { get; set; }
    }
}