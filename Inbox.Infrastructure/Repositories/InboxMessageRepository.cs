using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Inbox.Domain.Models;
using Inbox.Infrastructure.Abstraction.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Inbox.Infrastructure.Repositories
{
    public class InboxMessageRepository : IInboxMessageRepository
    {
        private readonly InboxContext _context;
        
        public InboxMessageRepository(InboxContext inboxContext)
        {
            _context = inboxContext;
        }

        public async Task<IEnumerable<InboxMessageModel>> FindAll()
        {
            return await _context.Messages.ToListAsync();
        }

        public async Task<InboxMessageModel> Find(Guid id)
        {
            return await _context.Messages.FindAsync(id);
        }

        public async Task Delete(Guid id)
        {
            var model = await Find(id);
            _context.Remove(model);
            await _context.SaveChangesAsync();
        }

        public async Task Update(InboxMessageModel model)
        {
            _context.Update(model);
            await _context.SaveChangesAsync();
        }

        public async Task Create(InboxMessageModel model)
        {
            await _context.Messages.AddAsync(model);
            await _context.SaveChangesAsync();
        }
    }
}