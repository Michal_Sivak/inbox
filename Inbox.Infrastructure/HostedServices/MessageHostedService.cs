using System;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Inbox.Infrastructure.Abstraction.Messaging;
using Inbox.Infrastructure.Events;
using Inbox.Infrastructure.Messaging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Inbox.Infrastructure.HostedServices
{
    public class MessageHostedService : IHostedService
    {
        private readonly IMessageReceiver _receiver;
        private readonly ILogger<MessageHostedService> _logger;
        private readonly IServiceProvider _provider;

        public MessageHostedService(IMessageReceiver messageReceiver, ILogger<MessageHostedService> logger,
            IServiceProvider serviceProvider)
        {
            _receiver = messageReceiver;
            _logger = logger;
            _provider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _receiver.MessageReceived += OnMessageReceived;
            return _receiver.ConsumeQueue();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _receiver.Dispose();
            return Task.CompletedTask;
        }

        private async Task OnMessageReceived(object sender, MessageEventArgs e)
        {
            var dtoType = GetDtoType(e.DtoType);
            if (dtoType == null)
                return;

            var message = ParseMessageToDto(e.Body, dtoType);
            if (message == null)
                return;

            var messageHandler = typeof(IMessageHandler<>).MakeGenericType(dtoType);
            
            using var scope = _provider.CreateScope();
            var handler = scope.ServiceProvider.GetService(messageHandler);
            if (handler == null)
            {
                throw new Exception($"Cannot find Handler for type '{messageHandler}'");
            }

            var handleMethod = messageHandler.GetMethod(nameof(IMessageHandler<object>.Handle), new[] { dtoType });
            if (handleMethod == null)
            {
                throw new Exception("Cannot find 'Handle' method on MessageHandler " +
                                    $"for message type '{dtoType.Name}'");   
            }

            if (!(handleMethod.Invoke(handler, new[] {message}) is Task task))
            {
                throw new Exception("Handle methods must return 'Task'");   
            }

            await task;
        }

        private Type GetDtoType(string dtoType)
        {
            try
            {
                return Type.GetType($"Inbox.Application.Dtos.{dtoType}, Inbox.Application", true);
            }
            catch (TypeLoadException ex)
            {
                _logger.LogWarning(ex, $"Cannot load type '{dtoType}'");
            }

            return null;
        }

        private object ParseMessageToDto(string message, Type type)
        {
            try
            {
                return JsonSerializer.Deserialize(message, type);
            }
            catch (ArgumentNullException ex)
            {
                _logger.LogError(ex, "Message body got from Messaging was null");
            }
            catch (JsonException ex)
            {
                _logger.LogError(ex, $"Cannot deserialize the body '{message}' to type '{type}'");
            }

            return null;
        }
    }
}