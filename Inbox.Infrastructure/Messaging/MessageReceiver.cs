using System;
using System.Text;
using System.Threading.Tasks;
using Inbox.Infrastructure.Configs;
using Inbox.Infrastructure.Events;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Inbox.Infrastructure.Messaging
{
    public class MessageReceiver : IMessageReceiver
    {
        private readonly ConnectionFactory _factory;
        private readonly string _queueName;

        private IModel _model;

        public event AsyncEventHandler<MessageEventArgs> MessageReceived;

        public MessageReceiver(ConnectionFactory factory, MessagingConfig messagingConfig)
        {
            _factory = factory;
            _queueName = messagingConfig.QueueName;
        }

        public Task ConsumeQueue()
        {
            var connection = _factory.CreateConnection();
            var model = connection.CreateModel();

            var consumer = new AsyncEventingBasicConsumer(model);
            consumer.Received += async (channel, eventArgs) =>
            {
                var dtoType = eventArgs.BasicProperties.Headers["DtoType"];
                var body = eventArgs.Body.ToArray();
                await OnMessageReceived(new MessageEventArgs()
                {
                    DtoType = Encoding.UTF8.GetString((byte[]) dtoType),
                    Body = Encoding.UTF8.GetString(body)
                });

                ((IModel) channel).BasicAck(eventArgs.DeliveryTag, false);
                await Task.Yield();
            };

            model.BasicConsume(_queueName, false, consumer);

            _model = model;
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _model.Dispose();
        }

        private async Task OnMessageReceived(MessageEventArgs e)
        {
            if (MessageReceived != null)
            {
                await MessageReceived.Invoke(this, e);   
            }
        }
    }
}