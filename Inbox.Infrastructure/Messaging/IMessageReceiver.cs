using System;
using System.Threading.Tasks;
using Inbox.Infrastructure.Events;
using RabbitMQ.Client.Events;

namespace Inbox.Infrastructure.Messaging
{
    public interface IMessageReceiver : IDisposable
    {
        Task ConsumeQueue();
        event AsyncEventHandler<MessageEventArgs> MessageReceived;
    }
}