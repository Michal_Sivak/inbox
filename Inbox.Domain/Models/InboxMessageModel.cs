using System;

namespace Inbox.Domain.Models
{
    public class InboxMessageModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime? DateRead { get; set; }
        public DateTime? DateArchived { get; set; }
    }
}