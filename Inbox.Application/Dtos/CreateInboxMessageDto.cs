namespace Inbox.Application.Dtos
{
    public class CreateInboxMessageDto
    {
        public string Title { get; set; }
        public string Content { get; set; }
    }
}