using System.Collections.Generic;

namespace Inbox.Application.Dtos
{
    public class InboxMessageListDto
    {
        public IEnumerable<InboxMessageDto> Items { get; set; }
    }
}