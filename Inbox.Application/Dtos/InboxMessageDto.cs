using System;

namespace Inbox.Application.Dtos
{
    public class InboxMessageDto
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime? DateRead { get; set; }
        public DateTime? DateArchived { get; set; }
    }
}