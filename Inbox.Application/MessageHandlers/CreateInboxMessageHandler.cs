using System.Threading.Tasks;
using Inbox.Application.Dtos;
using Inbox.Application.Services;
using Inbox.Infrastructure.Abstraction.Messaging;

namespace Inbox.Application.MessageHandlers
{
    public class CreateInboxMessageHandler : IMessageHandler<CreateInboxMessageDto>
    {
        private readonly IInboxService _inboxService;

        public CreateInboxMessageHandler(IInboxService inboxService)
        {
            _inboxService = inboxService;
        }

        public async Task Handle(CreateInboxMessageDto message)
        {
            await _inboxService.AddNew(message);
        }
    }
}