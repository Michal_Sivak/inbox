using Inbox.Application.Dtos;
using Inbox.Application.MessageHandlers;
using Inbox.Application.Services;
using Inbox.Infrastructure.Abstraction.Messaging;
using Microsoft.Extensions.DependencyInjection;

namespace Inbox.Application.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<IInboxService, InboxService>();
            services.AddSingleton<IDateTimeService, DateTimeService>();
            
            services.AddTransient<IMessageHandler<CreateInboxMessageDto>, CreateInboxMessageHandler>();

            return services;
        }
    }
}