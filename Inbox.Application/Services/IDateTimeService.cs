using System;

namespace Inbox.Application.Services
{
    public interface IDateTimeService
    {
        DateTime CurrentDateTime { get; }
    }
}