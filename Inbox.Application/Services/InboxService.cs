using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Inbox.Application.Dtos;
using Inbox.Domain.Models;
using Inbox.Infrastructure.Abstraction.Repositories;
using Inbox.Infrastructure.Repositories;

namespace Inbox.Application.Services
{
    public class InboxService : IInboxService
    {
        private readonly IInboxMessageRepository _repository;
        private readonly IDateTimeService _dateTimeService;

        public InboxService(IInboxMessageRepository repository, IDateTimeService dateTimeService)
        {
            _repository = repository;
            _dateTimeService = dateTimeService;
        }

        public async Task<InboxMessageListDto> GetAll()
        {
            var messages = await _repository.FindAll();
            
            var dtos = messages.Select(x => new InboxMessageDto()
            {
                Id = x.Id,
                Title = x.Title,
                Content = x.Content,
                DateRead = x.DateRead,
                DateArchived = x.DateArchived
            });

            return new InboxMessageListDto()
            {
                Items = dtos
            };
        }

        public async Task<InboxMessageDto> Get(Guid id)
        {
            var message = await _repository.Find(id);

            return new InboxMessageDto()
            {
                Id = message.Id,
                Title = message.Title,
                Content = message.Content,
                DateRead = message.DateRead,
                DateArchived = message.DateArchived
            };
        }

        public async Task MakeRead(Guid id)
        {
            var message = await _repository.Find(id);
            message.DateRead = _dateTimeService.CurrentDateTime;

            await _repository.Update(message);
        }

        public async Task MakeArchived(Guid id)
        {
            var message = await _repository.Find(id);
            message.DateArchived = _dateTimeService.CurrentDateTime;

            await _repository.Update(message);
        }

        public async Task AddNew(CreateInboxMessageDto dto)
        {
            var model = new InboxMessageModel()
            {
                Id = Guid.NewGuid(),
                Title = dto.Title,
                Content = dto.Content
            };
            
            await _repository.Create(model);
        }
    }
}