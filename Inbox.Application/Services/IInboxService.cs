using System;
using System.Threading.Tasks;
using Inbox.Application.Dtos;

namespace Inbox.Application.Services
{
    public interface IInboxService
    {
        Task<InboxMessageListDto> GetAll();
        Task<InboxMessageDto> Get(Guid id);
        Task MakeRead(Guid id);
        Task MakeArchived(Guid id);
        Task AddNew(CreateInboxMessageDto dto);
    }
}